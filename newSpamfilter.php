<?php

require 'vendor/autoload.php';

$spam = ["escort", "*escort*", "escorts", "viagra", "massage", "desi", "call girl", "call girls"];
$elasticConfig = array(
    'host' => 'search-cram-prod-644mijmby4qnstmkze4lout7a4.us-east-1.es.amazonaws.com',
    'port' => 80,
    'timeout' => 2.5,
    'elastic-scanscroll' => array(
        'scroll_ttl' => '1m',
        'results_per_shard' => '10',
    ),
);

$esUrl = 'http://' . $elasticConfig['host'] . ':' . $elasticConfig['port'] . '/fce_v1/sets/_search';


$mongoDbConfig = array(
    'host' => '127.0.0.1',
    'config' => '',
);

$start = 0;
$limit = 1000;

$MongoDbName = 'fce';
$MongoCollectionName = ['fc_set_spam_data', 'fc_set_spam_text'];

$log = new Logging();
$log_file = dirname(__FILE__) . '/log/' . date('y-m-d-h-i-s') . '_log.txt';
$log->lfile($log_file);

if (isset($argv) && is_array($argv) && isset($argv[1])) {
    // $spam = [$argv[1]];
    $spam = $argv[1];
    if (is_file($spam)) {
        $extension = end(explode('.', $spam));
        if ($extension == 'csv') {
            //$spam = array_map('str_getcsv', file($spam));
            $spam = readCsv($spam);
        } else {
            throw new Exception("Please provide a valid csv file");
        }
    }

    if (isset($argv[2])) {
        $start = $argv[2];
    }
}

if (empty($mongoDbConfig['config'])) {
    $m = new \MongoClient($mongoDbConfig['host']);
} else {
    $m = new \MongoClient($mongoDbConfig['host'], $mongoDbConfig['config']);
}
$collection = $m->selectCollection($MongoDbName, $MongoCollectionName[0]);
$collection->drop();
$collection2 = $m->selectCollection($MongoDbName, $MongoCollectionName[1]);
$collection2->drop();

//$es_query = '{"query":{"filtered":{"query":{"multi_match":{"fields":["title^100","front^10","back^5","subject^50","description"],"query":"%s"}},"filter":{"and":[{"term":{"access":"public"}}]}}},"fields":["setId","slug","title","subject","description","username","access","userId","views","score","hasImage","countCards","submitDate","lastModified"],"from":"%d","size":' . $limit . '}';

if (empty($spam)) {
    throw new Exception("Error: No spam words provided!");
}
if (is_string($spam)) {
    if (str_word_count($spam) > 1) {
        $spam = explode(' ', $spam);
    } else {
        $search_string = $spam;
    }
}
if (is_array($spam)) {
    if (count($spam) == 1) {
        $search_string = $spam[0];
    } else {
        $search_string = "'" . implode("', '", $spam) . "'";
    }
}

$scroll_id = null;

if (!empty($search_string)) {

    $documentsActive = 0;
    $documentsTotal = 0;
    $scrollId = null;

    $elasticSearchPaper = new ElasticSearchDocuments($elasticConfig['host']);
    $response = $elasticSearchPaper->scan($search_string, $limit);

    while (isset($response['hits']['hits']) && count($response['hits']['hits']) > 0) {

        $scanResult = $response['hits']['hits'];
        $SpmasRecords = count($scanResult) ;
        if($SpmasRecords>0){
            echo $SpmasRecords ." Spam Records Found!\n" ;
            $SpmasRecords = 0 ;
        }
        
        $insert = 0 ;

        foreach ($scanResult as $data) {

            if (!isset($data['fields']) || !is_array($data) || empty($data['fields'])) {
                echo 'Failed Record:' . json_encode($data);
            } else {

                $card = array(
                    'set_id' => $data['fields']['setId'][0],
                    'slug' => $data['fields']['slug'][0],
                    'front' => $data['fields']['front'][0],
                    'back' => $data['fields']['back'][0]
                );
                $flag = flagCard($card, $spam);

                if ($flag === true) {
                    $count = getSpamWord($data, $spam);
                    $insertResult = null;

                    if ($count[0] > 1) {
                        $document = array(
                            'set_id' => $data['fields']['setId'][0],
                            'slug' => $data['fields']['slug'][0],
                            'url' => 'http://www.cram.com/flashcards/' . $data['fields']['slug'][0],
                            'spam_frequency' => $count[0],
                            'spam_words' => json_encode($count[1]),
                                //'spam' => true
                        );
                        try {
                            $insertResult = $collection->insert($document, array("safe" => true));
                            $insert++;
                            if ($insert == 1) {
                                $collection->ensureIndex(array('set_id' => 1), array("unique" => true));
                            }

                            if (!empty($insertResult) && empty($insert['errmsg'])) {
                                try {
                                    $collection2->insert(
                                            [
                                                'set_id' => $data['fields']['setId'][0],
                                                'text' => $count['text']
                                            ]
                                    );
                                } catch (MongoCursorException $e) {
                                    $scriptexception = $e->getMessage();
                                    if (strpos($scriptexception, 'duplicate key error') === false) {
                                        echo "Error: " . $scriptexception . "\n";
                                    }
                                }
                            }
                        } catch (MongoCursorException $e) {
                            $scriptexception = $e->getMessage();
                            if (strpos($scriptexception, 'duplicate key error') === false) {
                                echo "Error: " . $scriptexception . "\n";
                            }
                        }
                    }
                }
            }
        } // end foreach       
        unset($scanResult);
        $scroll_id = $response['_scroll_id'];
        if($insert>0){
            echo $insert . " Spam Records Inserted Successfully. \n" ;
            $documentsTotal = $documentsTotal + $insert ;             
        }
        $response = $elasticSearchPaper->scroll($scroll_id, "1m");
        if($documentsTotal>0){
            $dbCount = $collection->count() ; 
            echo ">>>>>>>>>> Total Count = {$documentsTotal}, Database count = {$dbCount}\n";
        }
    }
}

if (!empty($documentsTotal)) {
    $output = "{$documentsTotal} spam records found.";
} else {
    $output = "No spam records found.";
}
echo $output . "\n";

# ################## No editing beyond this line #######################################

class ElasticSearchDocuments {

    protected $client;

    public function __construct($host) {
        $hosts = [
            [
                'host' => $host,
                'port' => '80',
                'scheme' => 'http',
            ],
        ];


        $this->client = Elasticsearch\ClientBuilder::create()->setHosts($hosts)->build();
    }

    public function scan($search_string, $limit) {

        $query = '{"query":{"filtered":{"query":{"multi_match":{"fields":["title^100","subject^99","description^98","front^95","back^95"],"query":"%s"}},"filter":{"and":[{"term":{"access":"public"}}]}}},"fields":["setId","slug","title","subject","description","front","back"]}';
        $elasticQuery = sprintf($query, $search_string);

        $params = [
            "scroll" => "1m", // how long between scroll requests. should be small!
            "size" => $limit, // how many results *per shard* you want back
            "index" => "fce_v1",
            "body" => $elasticQuery
        ];


        $response = $this->client->search($params);
        return $response;
    }

    public function scroll($scroll_id, $scroll) {
        $res = $this->client->scroll([
            "scroll_id" => $scroll_id,
            "scroll" => $scroll
                ]
        );

        return $res;
    }

}

function readCsv($f) {
    $csv = array();
    $file = fopen($f, 'r');

    while (($result = fgetcsv($file)) !== false) {
        if (is_array($result)) {
            $csv[] = $result[0];
        } else {
            $csv[] = $result;
        }
    }

    fclose($file);

    return $csv;
}

function flagCard($card, $aSpamKeywords) {
    $pattern = '/(' . implode('|', $aSpamKeywords) . ')/i';
    switch (true) {
        case (bool) preg_match($pattern, $card['front']):
        case (bool) preg_match($pattern, $card['back']):
            return true;
        default:
            return false;
    }
}

function getSpamWord($data, $spam) {

    $count = 0;
    $spam_word = [];

    $text = ' ';
    $text .= $data['fields']['title'][0] . ' ';
    $text .= $data['fields']['subject'][0] . ' ';
    $text .= $data['fields']['description'][0] . ' ';
    $text .= $data['fields']['back'][0] . ' ';
    $text .= $data['fields']['front'][0] . ' ';

    if (strlen($text) > 1000) {
        $text = substr($text, 0, 1000);
    }

    $text = escapeJsonString($text);
    $url = 'http://www.cram.com/flashcards/' . $data['fields']['slug'][0];

//    if (false === checkForSpam($text, $url)) {
//        return [$count];
//    }

    foreach ($spam as $string) {
        $search_string = '';
        if (str_word_count($string) > 1) {
            $search_string = $string;
        } else {
            $search_string = ' ' . $string;
        }

        $cnt = substr_count(strtolower($text), $search_string);
        if ($cnt) {
            array_push($spam_word, [$search_string, $cnt]);
        }

        $count += substr_count(strtolower($text), $search_string);
    }

    return [$count, $spam_word, 'text' => $text];
}

function escapeJsonString($value) {
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c", "'", "|", "�");
    $replacements = array("", "", "", "", "", "", "", "", "", "", "");
    $value = str_replace($escapers, $replacements, $value);
    $value = strip_tags($value);
    $value = ConvertToUTF8($value);
    return $value;
}

function ConvertToUTF8($text) {

    $encoding = mb_detect_encoding($text, mb_detect_order(), false);

    if ($encoding == "UTF-8") {
        $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');
    }

    $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);
    return $out;
}

function checkForSpam($text, $url) {
    global $log;
    if (strlen($text) > 1000) {
        $text = substr($text, 0, 1000);
    }

    $curl = curl_init();
    $data = ['api_key' => 'ea89dbc8c06f67d638e3ce240094c40c', 'text' => urlencode($text)];
    curl_setopt_array($curl, array(
        CURLOPT_PORT => "80",
        CURLOPT_URL => "http://api.datumbox.com:80/1.0/SpamDetection.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => http_build_query($data),
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: 8468cc23-69bf-685e-f703-b7ff480573f0"
        ),
    ));

    try {
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (empty($err) && !empty($response)) {
            $res = json_decode($response, TRUE);
            if (array_key_exists("result", $res["output"])) {
                if ($res["output"]["result"] == 'spam') {
                    return true;
                } else {
                    return false;
                }
            } else {
                $response['url'] = $url;
                $log->lwrite($response);
                return true;
            }
        }
    } catch (Exception $ex) {
        $error = ['url' => $url, 'err' => $ex->getMessage()];
        $log->lwrite($error);
        return true;
    }
}

class Logging {

    // declare log file and file pointer as private properties
    private $log_file, $fp;

    // set log file (path and name)
    public function lfile($path) {
        $this->log_file = $path;
    }

    // write message to the log file
    public function lwrite($message) {
        // if file pointer doesn't exist, then open log file
        if (!is_resource($this->fp)) {
            $this->lopen();
        }
        // define script name
        $script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
        // define current time and suppress E_WARNING if using the system TZ settings
        // (don't forget to set the INI setting date.timezone)
        $time = @date('[d/M/Y:H:i:s]');
        // write current time, script name and message to the log file
        fwrite($this->fp, "$time ($script_name) $message" . PHP_EOL);
    }

    // close log file (it's always a good idea to close a file when you're done with it)
    public function lclose() {
        fclose($this->fp);
    }

    // open log file (private method)
    private function lopen() {
        // in case of Windows set default log file
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $log_file_default = 'c:/php/logfile.txt';
        }
        // set default log file for Linux and other systems
        else {
            $log_file_default = dirname(__FILE__) . '/logfile.txt';
        }
        // define log file from lfile method or use previously set default
        $lfile = $this->log_file ? $this->log_file : $log_file_default;
        // open log file for writing only and place file pointer at the end of the file
        // (if the file does not exist, try to create it)
        $this->fp = fopen($lfile, 'a') or exit("Can't open $lfile!");
    }

}
