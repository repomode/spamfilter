<?php

/* * ************
 * @Author: Pranipat Khatua <pranipat@studymode.com>
 * ************ */
$elasticConfig = array(
    'host' => 'search-cram-prod-644mijmby4qnstmkze4lout7a4.us-east-1.es.amazonaws.com',
    'port' => 80,
    'timeout' => 2.5,
);

$esUrl = 'http://' . $elasticConfig['host'] . ':' . $elasticConfig['port'];

$mongo = new MongoClient("10.2.10.213");

$spamDb = $mongo->selectDB("fce");
$spamcollection = $spamDb->selectCollection("fc_set_spam_data_large");
$spamSearch = $spamDb->selectCollection("fc_set_spam_search");

$limit = 1000;
$del = 0;

if (isset($argv) && is_array($argv) && isset($argv[1])) {
    $set_id = $argv[1];
    delete_es($esUrl, $set_id);
    removeSet($set_id);
    //$spamcollection->remove(['set_id' => $set_id],["justOne" => true]);
} else {

    $cursor = $spamcollection->find([]);
    $total = $cursor->count();

    if ($total > 0) {
        echo $total . " Spam Records Found\n";
    } else {
        echo "No Spam data found to process \n";
        exit;
    }


    //exit;

    while ($del < $total) {

        $cursor2 = $spamcollection->find([], ['set_id']);
        $cursor2->limit($limit);
        $cursor2->timeout(-1);

        while ($cursor2->hasNext()) {

            $set = $cursor2->getNext();
            $set_id = $set['set_id'];
            echo "Deleting Set " . $set_id . "\n";

            if ($set_id) {
                delete_es($esUrl, $set_id);
                removeSet($set_id);
                $spamcollection->remove(['set_id' => $set_id], ["justOne" => true]);
                $spamSearch->remove(['set_id' => $set_id], ["justOne" => true]);
                $del++;
                if ($del > 0) {
                    echo "<< {$del}/{$total} Spam data has been removed from ES and Database .>> \n";
                }
            }
        }

        $limit = $limit + 100;
        if($del == $total){
            break ;
        }
    }
}

if ($del > 0) {
    echo "============================= Summary ==============================\n";
    echo "<< {$del} Spam data has been removed from ES and Database .>> \n";
}

function delete_es($esUrl, $set_id) {
    $esUrl = $esUrl . '/fce/sets/' . $set_id;
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $esUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "DELETE",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 2091b53f-0499-2035-671e-" . rand(1, 10)
        ),
    ));


    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err . "\n";
    } else {
        $res = json_decode($response, TRUE);
        if (!empty($res) && array_key_exists('found', $res) && $res['found'] == true) {
            echo "Set ID " . $set_id . " has been removed from ES\n";
        } else {
            "echo error while deleting set ID " . $set_id . "From ES\n";
        }
    }
}

function removeSet($set_id) {

    $m = new MongoClient("10.2.10.213");
    $db = $m->selectDB('cram');
    $collection = 'fc_set';

    $newdata = array('$set' => array("access" => "deleted", "spam" => true));
    $updateResult = $db->$collection->update(array("set_id" => (int) $set_id), $newdata);

    if (!empty($updateResult) && empty($insert['errmsg'])) {
        echo "Set ID " . $set_id . " has been removed from Database\n";
    } else {
        "echo error while deleting set ID " . $set_id . "From Database\n";
    }
}
