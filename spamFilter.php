<?php

require 'vendor/autoload.php';

$spam = ["helpido.com",
    "www.assignmentcloud.com",
    "www.coursehomework.com",
    "hwcampus.com",
    "helpido.com",
    "homeworktimes.com",
    "Homeworklance.com",
    "www.activitymode.com",
    "Www.studentoffortune.biz",
    "reportsnreports.com",
    "rnrmarketresearch.com",
    "marketreportschina.com",
    "marketresearchreports.biz",
    "sandlerresearch.org",
    "profresearchreports.com",
    "deepresearchreports.com",
    "researchmoz.com",
    "researchmoz.us",
    "Escort service",
    "Escorts service",
    "Escort services",
    "Outcall & In Call Escorts",
    "female Escorts",
    "female Escort",
    "call girl",
    "Call girls",
    "FULL BODY MASSAGE",
    "High Profile Escorts",
    "escort girl",
    "escort girls",
    "Escort female service",
    "Independent Escort",
    "High Class Housewife",
    "Sexy Erotic",
    "WhatsApp No",
    "whatsapp",
    "abortion pills",
    "Laxmi nagar",
    "astrologer baba",
    "Black magic",
    "black magic expert",
    "black magic specialist",
    "black magic spells",
    "LOVE AND MARRIAGE PROBLEM",
    "ASTROLOGY BABA",
    "ASTROLOGY SPECIALIST",
    "ASTROLOGY SPECIALIST",
    "online astrologer",
    "ONLINE ASTROLOGY",
    "baba ji",
    "TANTRIK",
    "vedik astrology",
    "vashikaran",
    "marriage astrology",
    "love astrology",
    "free astrology",
    "love solution astrology",
    "love problem specialist",
    "love spells",
    "love spell",
    "Lost love",
    "solve love and marriage problems",
    "Kala Jadoo",
    "love marriage specialist",
    "Click Link below To Purchase",
    "Click Link Below To Buy",
    "click the link below",
    "click link below",
    "Click here to Purchase",
    "To Purchase Visit link below",
    "To purchase this visit following link",
    "Buy a Copy Of This Report",
];

$elasticConfig = array(
    'host' => 'search-cram-prod-644mijmby4qnstmkze4lout7a4.us-east-1.es.amazonaws.com',
    'port' => 80,
    'timeout' => 2.5,
    'elastic-scanscroll' => array(
        'scroll_ttl' => '1m',
        'results_per_shard' => '10',
    ),
);

$esUrl = 'http://' . $elasticConfig['host'] . ':' . $elasticConfig['port'] . '/fce_v1/sets/_search';

$mongoDbConfig = array(
    'host' => '10.2.10.213',
    'config' => '',
);

$start = 0;
$limit = 1000;

$MongoDbName = 'fce';
$MongoCollectionName = ['fc_set_spam_data_large', 'fc_set_spam_text_large', 'fc_set_spam_search'];


if (empty($mongoDbConfig['config'])) {
    $m = new \MongoClient($mongoDbConfig['host']);
} else {
    $m = new \MongoClient($mongoDbConfig['host'], $mongoDbConfig['config']);
}
$collection = $m->selectCollection($MongoDbName, $MongoCollectionName[0]);
$collection2 = $m->selectCollection($MongoDbName, $MongoCollectionName[1]);
$collection3 = $m->selectCollection($MongoDbName, $MongoCollectionName[2]);

if (empty($spam)) {
    throw new Exception("Error: No spam words provided!");
    exit;
}

if (isset($argv) && is_array($argv) && isset($argv[1])) {
    // $spam = [$argv[1]];
    $spam = $argv[1];

    if (is_file($spam)) {
        $extension = end(explode('.', $spam));
        if ($extension == 'csv') {
            //$spam = array_map('str_getcsv', file($spam));
            $spam = readCsv($spam);
        } else {
            throw new Exception("Please provide a valid csv file");
        }
    } else if (is_string($spam)) {
        if (strpos($spam, ",") !== false) {
            $spam = explode(",", $spam);
        } else {
            $spam = [$spam];
        }
    }
}

if (is_array($spam)) {
    $search_string = "'" . implode("', '", $spam) . "'";
}

//$search_string = "'" . implode("', '", $spam) . "'";

echo "Searching for Spam words:" . $search_string . "\n";

$scroll_id = null;

if (!empty($search_string)) {

    $documentsActive = 0;
    $documentsTotal = 0;
    $scrollId = null;

    $elasticSearchPaper = new ElasticSearchDocuments($elasticConfig['host']);
    $response = $elasticSearchPaper->scan($search_string, $limit);

    while (isset($response['hits']['hits']) && count($response['hits']['hits']) > 0) {

        $scanResult = $response['hits']['hits'];
        $SpmasRecords = count($scanResult);
        if ($SpmasRecords > 0) {
            $documentsActive = $documentsActive + $SpmasRecords;
            echo $SpmasRecords . " Spam Records Found!";
            echo 'Total Scan:' . $documentsActive . "\n";
        }

        $insert = 0;

        foreach ($scanResult as $data) {

            if (!isset($data['fields']) || !is_array($data) || empty($data['fields'])) {
                echo 'Failed Record:' . json_encode($data);
            } else {

                $card = array(
                    'set_id' => $data['fields']['setId'][0],
                    'slug' => $data['fields']['slug'][0],
                    'title' => $data['fields']['back'][0],
                    'description' => $data['fields']['description'][0],
                    'subject' => $data['fields']['subject'][0],
                    'front' => $data['fields']['front'][0],
                    'back' => $data['fields']['back'][0]
                );



                $flag = (flagCardSpamText($card, $spam) === true) || (flagCardPhoneNumber($card) === true) ? true : false;

                unset($card['front']);
                unset($card['back']);
                unset($card['slug']);
                $card['url'] = 'http://www.cram.com/flashcards/' . $data['fields']['slug'][0];
                $card['Spam'] = ($flag === true) ? "True" : "False";
                $collection3->insert($card, array("safe" => true));

                if ($flag === true) {
                    $count = getSpamWord($data, $spam);
                    $insertResult = null;

                    if ($count[0] > 1) {
                        $document = array(
                            'set_id' => $data['fields']['setId'][0],
                            'slug' => $data['fields']['slug'][0],
                            'url' => 'http://www.cram.com/flashcards/' . $data['fields']['slug'][0],
                            'spam_frequency' => $count[0],
                            'spam_words' => json_encode($count[1]),
                                //'spam' => true
                        );
                        try {
                            $insertResult = $collection->insert($document, array("safe" => true));
                            $insert++;
                            if ($insert == 1) {
                                $collection->ensureIndex(array('set_id' => 1), array("unique" => true));
                            }
                        } catch (MongoCursorException $e) {
                            $scriptexception = $e->getMessage();
                            if (strpos($scriptexception, 'duplicate key error') === false) {
                                echo "Error: " . $scriptexception . "\n";
                            }
                        }
                    }
                }
            }
        } // end foreach       
        unset($scanResult);
        $scroll_id = $response['_scroll_id'];
        if ($insert > 0) {
            echo $insert . " Spam Records Inserted Successfully. \n";
            $documentsTotal = $documentsTotal + $insert;
        }
        $response = $elasticSearchPaper->scroll($scroll_id, "1m");
        if ($documentsTotal > 0) {
            $dbCount = $collection->count();
            echo ">>>>>>>>>> Total Insert Count = {$documentsTotal}, Database count = {$dbCount}\n";
        }
        $SpmasRecords = 0;
    }
}

if (!empty($documentsTotal)) {
    $output = "{$documentsTotal} spam records found.";
} else {
    $output = "No spam records found.";
}
echo $output . "\n";
unset($SpmasRecords);
unset($documentsActive);
unset($documentsTotal);
unset($insert);

# ################## No editing beyond this line #######################################

class ElasticSearchDocuments {

    protected $client;

    public function __construct($host) {
        $hosts = [
            [
                'host' => $host,
                'port' => '80',
                'scheme' => 'http',
            ],
        ];


        $this->client = Elasticsearch\ClientBuilder::create()->setHosts($hosts)->build();
    }

    public function scan($search_string, $limit) {

        $query = '{"query":{"filtered":{"query":{"multi_match":{"fields":["title^100","subject^99","description^98","front^95","back^95"],"query":"%s"}},"filter":{"and":[{"term":{"access":"public"}}]}}},"fields":["setId","slug","title","subject","description","front","back"]}';
        $elasticQuery = sprintf($query, $search_string);

        $params = [
            "scroll" => "1m", // how long between scroll requests. should be small!
            "size" => $limit, // how many results *per shard* you want back
            "index" => "fce_v1",
            "body" => $elasticQuery
        ];


        $response = $this->client->search($params);
        return $response;
    }

    public function scroll($scroll_id, $scroll) {
        $res = $this->client->scroll([
            "scroll_id" => $scroll_id,
            "scroll" => $scroll
                ]
        );

        return $res;
    }

}

function readCsv($f) {
    $csv = array();
    $file = fopen($f, 'r');

    while (($result = fgetcsv($file)) !== false) {
        if (is_array($result)) {
            $csv[] = $result[0];
        } else {
            $csv[] = $result;
        }
    }

    fclose($file);

    return $csv;
}

function flagCardSpamText($card, $aSpamKeywords) {
    $pattern = '/(' . implode('|', $aSpamKeywords) . ')/i';
    switch (true) {
        case (bool) preg_match($pattern, $card['subject']):
        case (bool) preg_match($pattern, $card['title']):
        case (bool) preg_match($pattern, $card['description']):
        case (bool) preg_match($pattern, $card['front']):
        case (bool) preg_match($pattern, $card['back']):
            return true;
        default:
            return false;
    }
}

function flagCardPhoneNumber($card) {
    /*
     * Universal phone number accepted
     * e.g.: +91 818248333, 1-818-248-7247, +1-818-248-7247, +91 32 45 55 22
     */
    $pattern = '/(?:\+(?:[0-9] ?){6,14}[0-9]|\+[0-9]{1,3}\.[0-9]{4,14}(?:x.+)?|\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})|\(?\b([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})\b|\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})|(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?)/u';
    switch (true) {
        case (bool) preg_match($pattern, $card['subject']):
        case (bool) preg_match($pattern, $card['title']):
        case (bool) preg_match($pattern, $card['description']):
        case (bool) preg_match($pattern, $card['front']):
        case (bool) preg_match($pattern, $card['back']):
            return true;
        default:
            return false;
    }
}

function getSpamWord($data, $spam) {

    $count = 0;
    $spam_word = [];

    $text = ' ';
    $text .= $data['fields']['subject'][0] . ' ';
    $text .= $data['fields']['title'][0] . ' ';
    $text .= $data['fields']['description'][0] . ' ';
    $text .= $data['fields']['back'][0] . ' ';
    $text .= $data['fields']['front'][0] . ' ';

    //$text = strtolower($text) ; 

    if (strlen($text) > 3000) {
        $text = substr($text, 0, 3000);
    }

    $text = escapeJsonString($text);


    foreach ($spam as $string) {
        $search_string = '';
        if (str_word_count($string) > 1) {
            $search_string = $string;
        } else {
            $search_string = ' ' . $string;
        }

        $cnt = substr_count(strtolower($text), strtolower($search_string));
        if ($cnt) {
            array_push($spam_word, [$search_string, $cnt]);
            $count += $cnt;
        }
    }

    return [$count, $spam_word];
}

function escapeJsonString($value) {
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c", "'", "|", "�");
    $replacements = array("", "", "", "", "", "", "", "", "", "", "");
    $value = str_replace($escapers, $replacements, $value);
    $value = strip_tags($value);
    $value = ConvertToUTF8($value);
    return $value;
}

function ConvertToUTF8($text) {

    $encoding = mb_detect_encoding($text, mb_detect_order(), false);

    if ($encoding == "UTF-8") {
        $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');
    }

    $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);
    return $out;
}
